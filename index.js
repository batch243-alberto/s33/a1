/*
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response=>response.json())
.then(data=>{
	data.map((elem)=> {
		console.log(elem.title)
	})
})*/
//*.then(json=> console.log(json));
const todoList = [];

fetch('https://jsonplaceholder.typicode.com/todos')
.then(response=>response.json())
.then(data=>{
	data.map((elem)=> {
		todoList.push(elem.title);
	})
});
console.log(todoList);


fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response=>response.json())
.then(json =>console.log(json))
.then(json =>console.log("The item detectus aut autem on the list has a status of false"))

fetch('https://jsonplaceholder.typicode.com/todos',{
		method: "POST",
		headers:{
			'Content-type':'application/json',
		},
		body: JSON.stringify({
			id:1,
			title: "Create to do list Item",
			completed: false,
			userId: 1
		})
	})
.then(response=>response.json())
.then(json=>console.log(json))


fetch('https://jsonplaceholder.typicode.com/todos/1',{
		method: "PUT",
		headers:{
			'Content-type':'application/json',
		},
		body: JSON.stringify({
			dataCompleted: "Pending",
			description: "To update my to do list with a different data structure",
			id: 1,
			status: "Pending",
			title: "Updated to Do List Item",
			userId: 1
		})
	})
.then(response=>response.json())
.then(json=>console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1',{
		method: "PATCH",
		headers:{
			'Content-type':'application/json',
		},
		body: JSON.stringify({
			completed: false,
			dataCompleted: "07/09/21",
			id: 1,
			status: "Complete",
			title: "detectus aut autem",
			userId: 1
		})
	})
.then(response=>response.json())
.then(json=>console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: "DELETE"
	})
